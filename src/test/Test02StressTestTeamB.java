package test;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import pages.TestPage;
import pages.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import test.ManipularExcel;

	public class Test02StressTestTeamB extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ; 
	

	/*
	int Loop = ManipularExcel.MAXIMO_FILAS;
	@DataProvider(name = "filas")
    private Object[] createData1() {
	 ArrayList<String> filas = new ArrayList<>();
		for(int i=3; i<= Loop; i++) {
			String C = String.valueOf(i);
			filas.add(C);
		}
	       return filas.toArray();
    }
    */  

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 30);
		TestBase TB = new TestBase(driver);
		TB.UrlDeLaWeb(apuntaA);
		
		
	}	
	
	@AfterMethod
	public void IrHome(ITestResult result) {
		
		if(ITestResult.FAILURE==result.getStatus()) 
		{ 
			BasePage.captureScreenshot(driver, BasePage.base__fechaActual("kk_mm_ss")); 
		}		 
		TestBase TB = new TestBase(driver); 
		TB.UrlDeLaWeb(apuntaA);
	}
	
	@AfterClass
	public void CloseChrome() {
		driver.quit();
		
	}   

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	

	
	/*************************************************************/
	/****************    SELECCIONA USUARIO    *******************/
	/*************************************************************/ 
	
	String Usuario21 ="usergenerate81@testgenerate.com"; 
	String Usuario22 ="usergenerate85@testgenerate.com"; 
	String Usuario23 ="usergenerate89@testgenerate.com"; 
	String Usuario24 ="usergenerate93@testgenerate.com"; 
	String Usuario25 ="usergenerate97@testgenerate.com"; 
	String Usuario26 ="usergenerate101@testgenerate.com"; 
	String Usuario27 ="usergenerate105@testgenerate.com"; 
	String Usuario28 ="usergenerate109@testgenerate.com"; 
	String Usuario29 ="usergenerate113@testgenerate.com"; 
	String Usuario30 ="usergenerate117@testgenerate.com"; 
	
	String Usuario31 ="usergenerate121@testgenerate.com"; 
	String Usuario32 ="usergenerate125@testgenerate.com"; 
	String Usuario33 ="usergenerate129@testgenerate.com"; 
	String Usuario34 ="usergenerate133@testgenerate.com"; 
	String Usuario35 ="usergenerate137@testgenerate.com"; 
	String Usuario36 ="usergenerate141@testgenerate.com"; 
	String Usuario37 ="usergenerate145@testgenerate.com"; 
	String Usuario38 ="usergenerate149@testgenerate.com"; 
	String Usuario39 ="usergenerate153@testgenerate.com"; 
	String Usuario40 ="usergenerate157@testgenerate.com"; 

	
	String passAmbiente ="123456789"; //Contraseña QA it@opratel.com  
	
	/*************************************************************/
	/************   SELECCION A QUE AMBIENTE APUNTA  *************/
	/*************************************************************/ 
	
	String ambiente = "todosgamers.com";
	
	
	//String apuntaA ="http://qa."; //AMBIENTE QA
	//String apuntaA ="http://dev4."; //AMBIENTE DEV7
	String apuntaA ="http://"; //AMBIENTE PRODUCCION
	
	
	/*************************************************************/
	/*************************************************************/
	/*************************************************************/
	
 

	 
 	
	
	@Test (priority=0)
	public void LogIn() { 		
		StressTestGrupoB TP = new StressTestGrupoB(driver);
		TP.LogInTodosGamers21(passAmbiente,Usuario21);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers22(passAmbiente,Usuario22);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers23(passAmbiente,Usuario23);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers24(passAmbiente,Usuario24);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers25(passAmbiente,Usuario25);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers26(passAmbiente,Usuario26);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers27(passAmbiente,Usuario27);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers28(passAmbiente,Usuario28);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers29(passAmbiente,Usuario29);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers30(passAmbiente,Usuario30);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers31(passAmbiente,Usuario31);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers32(passAmbiente,Usuario32);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers33(passAmbiente,Usuario33);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers34(passAmbiente,Usuario34);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers35(passAmbiente,Usuario35);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers36(passAmbiente,Usuario36);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers37(passAmbiente,Usuario37);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers38(passAmbiente,Usuario38);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers39(passAmbiente,Usuario39);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers40(passAmbiente,Usuario40);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
	}
	     
	
	
}
	
	
	/*Jenkins: http://54.163.143.224:8080/ 
	 * Usuario: qaauttg
	 * Contraseña: t0g4.0pr4
	 * */

package test;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import pages.TestPage;
import pages.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import test.ManipularExcel;

	public class Test01StressTestTeamA extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ; 
	

	/*
	int Loop = ManipularExcel.MAXIMO_FILAS;
	@DataProvider(name = "filas")
    private Object[] createData1() {
	 ArrayList<String> filas = new ArrayList<>();
		for(int i=3; i<= Loop; i++) {
			String C = String.valueOf(i);
			filas.add(C);
		}
	       return filas.toArray();
    }
    */  

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 30);
		TestBase TB = new TestBase(driver);
		TB.UrlDeLaWeb(apuntaA);
		
		
	}	
	
	@AfterMethod
	public void IrHome(ITestResult result) {
		
		if(ITestResult.FAILURE==result.getStatus()) 
		{ 
			BasePage.captureScreenshot(driver, BasePage.base__fechaActual("kk_mm_ss")); 
		}		 
		TestBase TB = new TestBase(driver); 
		TB.UrlDeLaWeb(apuntaA);
	}
	
	@AfterClass
	public void CloseChrome() {
		driver.quit();
		
	}   

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	

	
	/*************************************************************/
	/****************    SELECCIONA USUARIO    *******************/
	/*************************************************************/ 
		
	//String Usuario ="it@opratel.com";
	
	String Usuario1 ="usergenerate1@testgenerate.com"; 
	String Usuario2 ="usergenerate5@testgenerate.com"; 
	String Usuario3 ="usergenerate9@testgenerate.com"; 
	String Usuario4 ="usergenerate13@testgenerate.com"; 
	String Usuario5 ="usergenerate17@testgenerate.com"; 
	String Usuario6 ="usergenerate21@testgenerate.com"; 
	String Usuario7 ="usergenerate25@testgenerate.com"; 
	String Usuario8 ="usergenerate29@testgenerate.com"; 
	String Usuario9 ="usergenerate33@testgenerate.com"; 
	String Usuario10 ="usergenerate37@testgenerate.com";
	
	String Usuario11 ="usergenerate41@testgenerate.com"; 
	String Usuario12 ="usergenerate45@testgenerate.com"; 
	String Usuario13 ="usergenerate49@testgenerate.com"; 
	String Usuario14 ="usergenerate53@testgenerate.com"; 
	String Usuario15 ="usergenerate57@testgenerate.com"; 
	String Usuario16 ="usergenerate61@testgenerate.com"; 
	String Usuario17 ="usergenerate65@testgenerate.com"; 
	String Usuario18 ="usergenerate69@testgenerate.com"; 
	String Usuario19 ="usergenerate73@testgenerate.com"; 
	String Usuario20 ="usergenerate77@testgenerate.com"; 
	
	String passAmbiente ="123456789"; //Contraseña QA it@opratel.com
	//String passAmbiente ="72t1R!n5"; //Contraseña QA it@opratel.com
	
	/*************************************************************/
	/************   SELECCION A QUE AMBIENTE APUNTA  *************/
	/*************************************************************/ 
	
	String ambiente = "todosgamers.com";
	
	
	//String apuntaA ="http://qa."; //AMBIENTE QA
	//String apuntaA ="http://dev4."; //AMBIENTE DEV7
	String apuntaA ="http://"; //AMBIENTE PRODUCCION
	
	
	
	
	/*************************************************************/
	/*************************************************************/
	/*************************************************************/
	
 

	 
 	
	
	@Test (priority=0)
	public void LogIn() { 		
		StressTestGrupoA TP = new StressTestGrupoA(driver);
		TP.LogInTodosGamers1(passAmbiente,Usuario1);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers2(passAmbiente,Usuario2);
		TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers3(passAmbiente,Usuario3);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers4(passAmbiente,Usuario4);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers5(passAmbiente,Usuario5);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers6(passAmbiente,Usuario6);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers7(passAmbiente,Usuario7);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers8(passAmbiente,Usuario8);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers9(passAmbiente,Usuario9);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers10(passAmbiente,Usuario10);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers11(passAmbiente,Usuario11);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers12(passAmbiente,Usuario12);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers13(passAmbiente,Usuario13);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers14(passAmbiente,Usuario14);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers15(passAmbiente,Usuario15);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers16(passAmbiente,Usuario16);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers17(passAmbiente,Usuario17);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers18(passAmbiente,Usuario18);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers19(passAmbiente,Usuario19);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers20(passAmbiente,Usuario20);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
	}
	     
	
}
	
	
	/*Jenkins: http://54.163.143.224:8080/ 
	 * Usuario: qaauttg
	 * Contraseña: t0g4.0pr4
	 * */

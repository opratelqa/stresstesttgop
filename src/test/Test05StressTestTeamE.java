package test;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import pages.TestPage;
import pages.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import test.ManipularExcel;

	public class Test05StressTestTeamE extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ; 
	

	/*
	int Loop = ManipularExcel.MAXIMO_FILAS;
	@DataProvider(name = "filas")
    private Object[] createData1() {
	 ArrayList<String> filas = new ArrayList<>();
		for(int i=3; i<= Loop; i++) {
			String C = String.valueOf(i);
			filas.add(C);
		}
	       return filas.toArray();
    }
    */  

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 30);
		TestBase TB = new TestBase(driver);
		TB.UrlDeLaWeb(apuntaA);
		
		
	}	
	
	@AfterMethod
	public void IrHome(ITestResult result) {
		
		if(ITestResult.FAILURE==result.getStatus()) 
		{ 
			BasePage.captureScreenshot(driver, BasePage.base__fechaActual("kk_mm_ss")); 
		}		 
		TestBase TB = new TestBase(driver); 
		TB.UrlDeLaWeb(apuntaA);
	}
	
	@AfterClass
	public void CloseChrome() {
		driver.quit();
		
	}   

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	

	
	/*************************************************************/
	/****************    SELECCIONA USUARIO    *******************/
	/*************************************************************/ 
	
	String Usuario81 ="usergenerate321@testgenerate.com"; 
	String Usuario82 ="usergenerate325@testgenerate.com"; 
	String Usuario83 ="usergenerate329@testgenerate.com"; 
	String Usuario84 ="usergenerate333@testgenerate.com"; 
	String Usuario85 ="usergenerate337@testgenerate.com"; 
	String Usuario86 ="usergenerate341@testgenerate.com"; 
	String Usuario87 ="usergenerate345@testgenerate.com"; 
	String Usuario88 ="usergenerate349@testgenerate.com"; 
	String Usuario89 ="usergenerate353@testgenerate.com"; 
	String Usuario90 ="usergenerate357@testgenerate.com"; 
	
	String Usuario91 ="usergenerate361@testgenerate.com"; 
	String Usuario92 ="usergenerate365@testgenerate.com"; 
	String Usuario93 ="usergenerate369@testgenerate.com"; 
	String Usuario94 ="usergenerate373@testgenerate.com"; 
	String Usuario95 ="usergenerate377@testgenerate.com"; 
	String Usuario96 ="usergenerate381@testgenerate.com"; 
	String Usuario97 ="usergenerate385@testgenerate.com"; 
	String Usuario98 ="usergenerate389@testgenerate.com"; 
	String Usuario99 ="usergenerate393@testgenerate.com"; 
	String Usuario100 ="usergenerate397@testgenerate.com"; 

	
	String passAmbiente ="123456789"; //Contraseña QA it@opratel.com  
	
	/*************************************************************/
	/************   SELECCION A QUE AMBIENTE APUNTA  *************/
	/*************************************************************/ 
	
	String ambiente = "todosgamers.com";
	
	
	//String apuntaA ="http://qa."; //AMBIENTE QA
	//String apuntaA ="http://dev4."; //AMBIENTE DEV7
	String apuntaA ="http://"; //AMBIENTE PRODUCCION
	
	
	
	
	/*************************************************************/
	/*************************************************************/
	/*************************************************************/
	
 

	 
 	
	
	@Test (priority=0)
	public void LogIn() { 		
		StressTestGrupoE TP = new StressTestGrupoE(driver);
		TP.LogInTodosGamers81(passAmbiente,Usuario81);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers82(passAmbiente,Usuario82);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers83(passAmbiente,Usuario83);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers84(passAmbiente,Usuario84);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers85(passAmbiente,Usuario85);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers86(passAmbiente,Usuario86);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers87(passAmbiente,Usuario87);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers88(passAmbiente,Usuario88);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers89(passAmbiente,Usuario89);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers90(passAmbiente,Usuario90);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers91(passAmbiente,Usuario91);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers92(passAmbiente,Usuario92);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers93(passAmbiente,Usuario93);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers94(passAmbiente,Usuario94);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers95(passAmbiente,Usuario95);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers96(passAmbiente,Usuario96);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers97(passAmbiente,Usuario97);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers98(passAmbiente,Usuario98);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers99(passAmbiente,Usuario99);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers100(passAmbiente,Usuario100);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
	}
	     
	
	
}
	
	
	/*Jenkins: http://54.163.143.224:8080/ 
	 * Usuario: qaauttg
	 * Contraseña: t0g4.0pr4
	 * */

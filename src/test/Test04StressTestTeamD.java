package test;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import pages.TestPage;
import pages.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import test.ManipularExcel;

	public class Test04StressTestTeamD extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ; 
	

	/*
	int Loop = ManipularExcel.MAXIMO_FILAS;
	@DataProvider(name = "filas")
    private Object[] createData1() {
	 ArrayList<String> filas = new ArrayList<>();
		for(int i=3; i<= Loop; i++) {
			String C = String.valueOf(i);
			filas.add(C);
		}
	       return filas.toArray();
    }
    */  

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 30);
		TestBase TB = new TestBase(driver);
		TB.UrlDeLaWeb(apuntaA);
		
		
	}	
	
	@AfterMethod
	public void IrHome(ITestResult result) {
		
		if(ITestResult.FAILURE==result.getStatus()) 
		{ 
			BasePage.captureScreenshot(driver, BasePage.base__fechaActual("kk_mm_ss")); 
		}		 
		TestBase TB = new TestBase(driver); 
		TB.UrlDeLaWeb(apuntaA);
	}
	
	@AfterClass
	public void CloseChrome() {
		driver.quit();
		
	}   

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	

	
	/*************************************************************/
	/****************    SELECCIONA USUARIO    *******************/
	/*************************************************************/ 
	
	String Usuario61 ="usergenerate241@testgenerate.com"; 
	String Usuario62 ="usergenerate245@testgenerate.com"; 
	String Usuario63 ="usergenerate249@testgenerate.com"; 
	String Usuario64 ="usergenerate253@testgenerate.com"; 
	String Usuario65 ="usergenerate257@testgenerate.com"; 
	String Usuario66 ="usergenerate261@testgenerate.com"; 
	String Usuario67 ="usergenerate265@testgenerate.com"; 
	String Usuario68 ="usergenerate269@testgenerate.com"; 
	String Usuario69 ="usergenerate273@testgenerate.com"; 
	String Usuario70 ="usergenerate277@testgenerate.com"; 
	
	String Usuario71 ="usergenerate281@testgenerate.com"; 
	String Usuario72 ="usergenerate285@testgenerate.com"; 
	String Usuario73 ="usergenerate289@testgenerate.com"; 
	String Usuario74 ="usergenerate293@testgenerate.com"; 
	String Usuario75 ="usergenerate297@testgenerate.com"; 
	String Usuario76 ="usergenerate301@testgenerate.com"; 
	String Usuario77 ="usergenerate305@testgenerate.com"; 
	String Usuario78 ="usergenerate309@testgenerate.com"; 
	String Usuario79 ="usergenerate313@testgenerate.com"; 
	String Usuario80 ="usergenerate317@testgenerate.com"; 

	
	String passAmbiente ="123456789"; //Contraseña QA it@opratel.com  
	
	/*************************************************************/
	/************   SELECCION A QUE AMBIENTE APUNTA  *************/
	/*************************************************************/ 
	
	String ambiente = "todosgamers.com";
	
	
	//String apuntaA ="http://qa."; //AMBIENTE QA
	//String apuntaA ="http://dev4."; //AMBIENTE DEV7
	String apuntaA ="http://"; //AMBIENTE PRODUCCION
	
	
	
	
	/*************************************************************/
	/*************************************************************/
	/*************************************************************/
	
 

	 
 	
	
	@Test (priority=0)
	public void LogIn() { 		
		StressTestGrupoD TP = new StressTestGrupoD(driver);
		TP.LogInTodosGamers61(passAmbiente,Usuario61);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers62(passAmbiente,Usuario62);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers63(passAmbiente,Usuario63);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers64(passAmbiente,Usuario64);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers65(passAmbiente,Usuario65);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers66(passAmbiente,Usuario66);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers67(passAmbiente,Usuario67);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers68(passAmbiente,Usuario68);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers69(passAmbiente,Usuario69);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers70(passAmbiente,Usuario70);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers71(passAmbiente,Usuario71);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers72(passAmbiente,Usuario72);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers73(passAmbiente,Usuario73);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers74(passAmbiente,Usuario74);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers75(passAmbiente,Usuario75);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers76(passAmbiente,Usuario76);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers77(passAmbiente,Usuario77);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers78(passAmbiente,Usuario78);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers79(passAmbiente,Usuario79);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers80(passAmbiente,Usuario80);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
	}
	     
	
	
}
	
	
	/*Jenkins: http://54.163.143.224:8080/ 
	 * Usuario: qaauttg
	 * Contraseña: t0g4.0pr4
	 * */

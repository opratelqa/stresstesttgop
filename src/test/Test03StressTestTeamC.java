package test;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import pages.TestPage;
import pages.*;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDFont;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import test.ManipularExcel;

	public class Test03StressTestTeamC extends BasePage {

	protected WebDriver driver;
	protected  WebDriverWait wait;
	String usuario ;
	String paswd ; 
	

	/*
	int Loop = ManipularExcel.MAXIMO_FILAS;
	@DataProvider(name = "filas")
    private Object[] createData1() {
	 ArrayList<String> filas = new ArrayList<>();
		for(int i=3; i<= Loop; i++) {
			String C = String.valueOf(i);
			filas.add(C);
		}
	       return filas.toArray();
    }
    */  

	@BeforeClass
	public void IngresoAmbiente() {
		this.driver = setConexion.setup();
		wait = new WebDriverWait(driver, 30);
		TestBase TB = new TestBase(driver);
		TB.UrlDeLaWeb(apuntaA);
		
		
	}	
	
	@AfterMethod
	public void IrHome(ITestResult result) {
		
		if(ITestResult.FAILURE==result.getStatus()) 
		{ 
			BasePage.captureScreenshot(driver, BasePage.base__fechaActual("kk_mm_ss")); 
		}		 
		TestBase TB = new TestBase(driver); 
		TB.UrlDeLaWeb(apuntaA);
	}
	
	@AfterClass
	public void CloseChrome() {
		driver.quit();
		
	}   

	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}
	}
	

	
	/*************************************************************/
	/****************    SELECCIONA USUARIO    *******************/
	/*************************************************************/ 
	
	String Usuario41 ="usergenerate161@testgenerate.com"; 
	String Usuario42 ="usergenerate165@testgenerate.com"; 
	String Usuario43 ="usergenerate169@testgenerate.com"; 
	String Usuario44 ="usergenerate173@testgenerate.com"; 
	String Usuario45 ="usergenerate177@testgenerate.com"; 
	String Usuario46 ="usergenerate181@testgenerate.com"; 
	String Usuario47 ="usergenerate185@testgenerate.com"; 
	String Usuario48 ="usergenerate189@testgenerate.com"; 
	String Usuario49 ="usergenerate193@testgenerate.com"; 
	String Usuario50 ="usergenerate197@testgenerate.com"; 
	
	String Usuario51 ="usergenerate201@testgenerate.com"; 
	String Usuario52 ="usergenerate205@testgenerate.com"; 
	String Usuario53 ="usergenerate209@testgenerate.com"; 
	String Usuario54 ="usergenerate213@testgenerate.com"; 
	String Usuario55 ="usergenerate217@testgenerate.com"; 
	String Usuario56 ="usergenerate221@testgenerate.com"; 
	String Usuario57 ="usergenerate225@testgenerate.com"; 
	String Usuario58 ="usergenerate229@testgenerate.com"; 
	String Usuario59 ="usergenerate233@testgenerate.com"; 
	String Usuario60 ="usergenerate237@testgenerate.com"; 

	
	String passAmbiente ="123456789"; //Contraseña QA it@opratel.com  
	
	/*************************************************************/
	/************   SELECCION A QUE AMBIENTE APUNTA  *************/
	/*************************************************************/ 
	
	String ambiente = "todosgamers.com";
	
	
	//String apuntaA ="http://qa."; //AMBIENTE QA
	//String apuntaA ="http://dev4."; //AMBIENTE DEV7
	String apuntaA ="http://"; //AMBIENTE PRODUCCION
	
	
	
	
	/*************************************************************/
	/*************************************************************/
	/*************************************************************/
	
 

	 
 	
	
	@Test (priority=0)
	public void LogIn() { 		
		StressTestGrupoC TP = new StressTestGrupoC(driver);
		TP.LogInTodosGamers41(passAmbiente,Usuario41);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers42(passAmbiente,Usuario42);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers43(passAmbiente,Usuario43);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers44(passAmbiente,Usuario44);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers45(passAmbiente,Usuario45);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers46(passAmbiente,Usuario46);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers47(passAmbiente,Usuario47);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers48(passAmbiente,Usuario48);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers49(passAmbiente,Usuario49);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers50(passAmbiente,Usuario50);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers51(passAmbiente,Usuario51);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers52(passAmbiente,Usuario52);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers53(passAmbiente,Usuario53);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers54(passAmbiente,Usuario54);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers55(passAmbiente,Usuario55);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers56(passAmbiente,Usuario56);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers57(passAmbiente,Usuario57);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers58(passAmbiente,Usuario58);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers59(passAmbiente,Usuario59);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
		TP.LogInTodosGamers60(passAmbiente,Usuario60);
		//TP.InscripcionTorneo();
		TP.LogOutTodosGamers(apuntaA);
		
	}
	     
	
	
}
	
	
	/*Jenkins: http://54.163.143.224:8080/ 
	 * Usuario: qaauttg
	 * Contraseña: t0g4.0pr4
	 * */

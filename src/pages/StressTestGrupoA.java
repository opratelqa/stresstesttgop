package pages;

 
import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
  
public class StressTestGrupoA extends TestBaseTG {
	
	final WebDriver driver; 
	public StressTestGrupoA(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	 
	
	  
	/*
	 ******PASAR A BASEPAGE
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	 
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "email_reg")
	private WebElement nombreUsuario;
	
	@FindBy(how = How.ID,using = "password_reg")
	private WebElement contraseñaUsuario;
	
	@FindBy(how = How.ID,using = "container-chat-open-button")
	private WebElement chatOnline;

	 
	//***************************** 

	 
	public void LogInTodosGamers1(String passAmbiente, String Usuario1) {
		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test StressTest-LogIn");
		 
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000); 
		driver.switchTo();
			nombreUsuario.sendKeys(Usuario1);
			contraseñaUsuario.sendKeys(passAmbiente);
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	   
		
		//UNIR AL TORNEO
		
	 
	}
	
public void LogInTodosGamers2(String passAmbiente, String Usuario2) {
		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test StressTest-LogIn");
		 
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000); 
		driver.switchTo();
			nombreUsuario.sendKeys(Usuario2);
			contraseñaUsuario.sendKeys(passAmbiente);
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	   
		
		//UNIR AL TORNEO
		
	 
	}

public void LogInTodosGamers3(String passAmbiente, String Usuario3) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario3);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers4(String passAmbiente, String Usuario4) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario4);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}
	

public void LogInTodosGamers5(String passAmbiente, String Usuario5) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario5);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers6(String passAmbiente, String Usuario6) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario6);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}
	

public void LogInTodosGamers7(String passAmbiente, String Usuario7) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario7);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers8(String passAmbiente, String Usuario8) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario8);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers9(String passAmbiente, String Usuario9) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario9);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers10(String passAmbiente, String Usuario10) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario10);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers11(String passAmbiente, String Usuario11) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario11);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers12(String passAmbiente, String Usuario12) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario12);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers13(String passAmbiente, String Usuario13) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario13);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers14(String passAmbiente, String Usuario14) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario14);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers15(String passAmbiente, String Usuario15) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario15);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers16(String passAmbiente, String Usuario16) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario16);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers17(String passAmbiente, String Usuario17) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario17);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers18(String passAmbiente, String Usuario18) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario18);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers19(String passAmbiente, String Usuario19) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario19);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers20(String passAmbiente, String Usuario20) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario20);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


	public void InscripcionTorneo() {
		espera(500);
		driver.get("https://todosgamers.com/tournaments/Lorem2445/details");
		
		WebElement btnRegistrarse = driver.findElement(By.xpath("//a[contains(text(), ' Registrarse')]"));
			btnRegistrarse.click();
			espera(900);
			
			WebElement contraseña = driver.findElement(By.id("password_btn"));
			contraseña.sendKeys("nintendo64");
			espera(900);
			
			WebElement confirmar = driver.findElement(By.id("modal-confirm-btn-confirm"));
			confirmar.click();
			espera(900);
			
}

	public void LogOutTodosGamers(String apuntaA) {	
		cargando(500); 
			espera(500);
			//WebElement btnLogOut = driver.findElement(By.cssSelector(".icon-logout"));
			//btnLogOut.click();
			driver.get(apuntaA + "todosgamers.com/logout");
			cargando(500); 
		espera(500);	
		System.out.println("Log out");
	}	
	
}
 
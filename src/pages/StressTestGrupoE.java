package pages;

 
import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
  
public class StressTestGrupoE extends TestBaseTG {
	
	final WebDriver driver; 
	public StressTestGrupoE(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	 
	
	  
	/*
	 ******PASAR A BASEPAGE
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	 
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "email_reg")
	private WebElement nombreUsuario;
	
	@FindBy(how = How.ID,using = "password_reg")
	private WebElement contraseñaUsuario;
	
	@FindBy(how = How.ID,using = "container-chat-open-button")
	private WebElement chatOnline;

	 
	//***************************** 

	 
	public void LogInTodosGamers81(String passAmbiente, String Usuario81) {
		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test StressTest-LogIn");
		 
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000); 
		driver.switchTo();
			nombreUsuario.sendKeys(Usuario81);
			contraseñaUsuario.sendKeys(passAmbiente);
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	   
		
		//UNIR AL TORNEO
		
	 
	}
	
public void LogInTodosGamers82(String passAmbiente, String Usuario82) {
		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test StressTest-LogIn");
		 
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000); 
		driver.switchTo();
			nombreUsuario.sendKeys(Usuario82);
			contraseñaUsuario.sendKeys(passAmbiente);
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	   
		
		//UNIR AL TORNEO
		
	 
	}

public void LogInTodosGamers83(String passAmbiente, String Usuario83) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario83);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers84(String passAmbiente, String Usuario84) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario84);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}
	

public void LogInTodosGamers85(String passAmbiente, String Usuario85) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario85);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers86(String passAmbiente, String Usuario86) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario86);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}
	

public void LogInTodosGamers87(String passAmbiente, String Usuario87) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario87);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers88(String passAmbiente, String Usuario88) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario88);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers89(String passAmbiente, String Usuario89) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario89);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers90(String passAmbiente, String Usuario90) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario90);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers91(String passAmbiente, String Usuario91) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario91);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers92(String passAmbiente, String Usuario92) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario92);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers93(String passAmbiente, String Usuario93) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario93);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers94(String passAmbiente, String Usuario94) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario94);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers95(String passAmbiente, String Usuario95) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario95);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers96(String passAmbiente, String Usuario96) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario96);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers97(String passAmbiente, String Usuario97) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario97);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers98(String passAmbiente, String Usuario98) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario98);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers99(String passAmbiente, String Usuario99) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario99);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers100(String passAmbiente, String Usuario100) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario100);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void InscripcionTorneo() {
	espera(500);
	driver.get("https://todosgamers.com/tournaments/Lorem2445/details");
	
	WebElement btnRegistrarse = driver.findElement(By.xpath("//a[contains(text(), ' Registrarse')]"));
		btnRegistrarse.click();
		espera(900);
		
		WebElement contraseña = driver.findElement(By.id("password_btn"));
		contraseña.sendKeys("nintendo64");
		espera(900);
		
		WebElement confirmar = driver.findElement(By.id("modal-confirm-btn-confirm"));
		confirmar.click();
		espera(900);
		
}

	public void LogOutTodosGamers(String apuntaA) {	
		cargando(500); 
			espera(500);
			//WebElement btnLogOut = driver.findElement(By.cssSelector(".icon-logout"));
			//btnLogOut.click();
			driver.get(apuntaA + "todosgamers.com/logout");
			cargando(500); 
		espera(500);	
		System.out.println("Log out");
	}	
	
}
 
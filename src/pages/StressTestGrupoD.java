package pages;

 
import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
  
public class StressTestGrupoD extends TestBaseTG {
	
	final WebDriver driver; 
	public StressTestGrupoD(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	 
	
	  
	/*
	 ******PASAR A BASEPAGE
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	 
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "email_reg")
	private WebElement nombreUsuario;
	
	@FindBy(how = How.ID,using = "password_reg")
	private WebElement contraseñaUsuario;
	
	@FindBy(how = How.ID,using = "container-chat-open-button")
	private WebElement chatOnline;

	 
	//***************************** 

	 
	public void LogInTodosGamers61(String passAmbiente, String Usuario61) {
		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test StressTest-LogIn");
		 
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000); 
		driver.switchTo();
			nombreUsuario.sendKeys(Usuario61);
			contraseñaUsuario.sendKeys(passAmbiente);
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	   
		
		//UNIR AL TORNEO
		
	 
	}
	
public void LogInTodosGamers62(String passAmbiente, String Usuario62) {
		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test StressTest-LogIn");
		 
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000); 
		driver.switchTo();
			nombreUsuario.sendKeys(Usuario62);
			contraseñaUsuario.sendKeys(passAmbiente);
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	   
		
		//UNIR AL TORNEO
		
	 
	}

public void LogInTodosGamers63(String passAmbiente, String Usuario63) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario63);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers64(String passAmbiente, String Usuario64) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario64);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}
	

public void LogInTodosGamers65(String passAmbiente, String Usuario65) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario65);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers66(String passAmbiente, String Usuario66) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario66);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}
	

public void LogInTodosGamers67(String passAmbiente, String Usuario67) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario67);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers68(String passAmbiente, String Usuario68) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario68);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers69(String passAmbiente, String Usuario69) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario69);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers70(String passAmbiente, String Usuario70) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario70);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers71(String passAmbiente, String Usuario71) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario71);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers72(String passAmbiente, String Usuario72) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario72);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers73(String passAmbiente, String Usuario73) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario73);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers74(String passAmbiente, String Usuario74) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario74);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers75(String passAmbiente, String Usuario75) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario75);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers76(String passAmbiente, String Usuario76) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario76);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers77(String passAmbiente, String Usuario77) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario77);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers78(String passAmbiente, String Usuario78) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario78);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers79(String passAmbiente, String Usuario79) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario79);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers80(String passAmbiente, String Usuario80) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario80);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void InscripcionTorneo() {
	espera(500);
	driver.get("https://todosgamers.com/tournaments/Lorem2445/details");
	
	WebElement btnRegistrarse = driver.findElement(By.xpath("//a[contains(text(), ' Registrarse')]"));
		btnRegistrarse.click();
		espera(900);
		
		WebElement contraseña = driver.findElement(By.id("password_btn"));
		contraseña.sendKeys("nintendo64");
		espera(900);
		
		WebElement confirmar = driver.findElement(By.id("modal-confirm-btn-confirm"));
		confirmar.click();
		espera(900);
		
}

	public void LogOutTodosGamers(String apuntaA) {	
		cargando(500); 
			espera(500);
			//WebElement btnLogOut = driver.findElement(By.cssSelector(".icon-logout"));
			//btnLogOut.click();
			driver.get(apuntaA + "todosgamers.com/logout");
			cargando(500); 
		espera(500);	
		System.out.println("Log out");
	}	
	
}
 
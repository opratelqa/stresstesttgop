package pages;

 
import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
  
public class StressTestGrupoB extends TestBaseTG {
	
	final WebDriver driver; 
	public StressTestGrupoB(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	 
	
	  
	/*
	 ******PASAR A BASEPAGE
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	 
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "email_reg")
	private WebElement nombreUsuario;
	
	@FindBy(how = How.ID,using = "password_reg")
	private WebElement contraseñaUsuario;
	
	@FindBy(how = How.ID,using = "container-chat-open-button")
	private WebElement chatOnline;

	 
	//***************************** 

	 
	public void LogInTodosGamers21(String passAmbiente, String Usuario21) {
		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test StressTest-LogIn");
		 
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000); 
		driver.switchTo();
			nombreUsuario.sendKeys(Usuario21);
			contraseñaUsuario.sendKeys(passAmbiente);
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	   
		
		//UNIR AL TORNEO
		
	 
	}
	
public void LogInTodosGamers22(String passAmbiente, String Usuario22) {
		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test StressTest-LogIn");
		 
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000); 
		driver.switchTo();
			nombreUsuario.sendKeys(Usuario22);
			contraseñaUsuario.sendKeys(passAmbiente);
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	   
		
		//UNIR AL TORNEO
		
	 
	}

public void LogInTodosGamers23(String passAmbiente, String Usuario23) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario23);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers24(String passAmbiente, String Usuario24) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario24);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}
	

public void LogInTodosGamers25(String passAmbiente, String Usuario25) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario25);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers26(String passAmbiente, String Usuario26) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario26);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}
	

public void LogInTodosGamers27(String passAmbiente, String Usuario27) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario27);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers28(String passAmbiente, String Usuario28) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario28);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers29(String passAmbiente, String Usuario29) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario29);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers30(String passAmbiente, String Usuario30) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario30);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers31(String passAmbiente, String Usuario31) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario31);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers32(String passAmbiente, String Usuario32) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario32);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers33(String passAmbiente, String Usuario33) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario33);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers34(String passAmbiente, String Usuario34) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario34);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers35(String passAmbiente, String Usuario35) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario35);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers36(String passAmbiente, String Usuario36) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario36);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers37(String passAmbiente, String Usuario37) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario37);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers38(String passAmbiente, String Usuario38) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario38);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers39(String passAmbiente, String Usuario39) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario39);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers40(String passAmbiente, String Usuario40) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario40);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void InscripcionTorneo() {
	espera(500);
	driver.get("https://todosgamers.com/tournaments/Lorem2445/details");
	
	WebElement btnRegistrarse = driver.findElement(By.xpath("//a[contains(text(), ' Registrarse')]"));
		btnRegistrarse.click();
		espera(900);
		
		WebElement contraseña = driver.findElement(By.id("password_btn"));
		contraseña.sendKeys("nintendo64");
		espera(900);
		
		WebElement confirmar = driver.findElement(By.id("modal-confirm-btn-confirm"));
		confirmar.click();
		espera(900);
		
}

	public void LogOutTodosGamers(String apuntaA) {	
		cargando(500); 
			espera(500);
			//WebElement btnLogOut = driver.findElement(By.cssSelector(".icon-logout"));
			//btnLogOut.click();
			driver.get(apuntaA + "todosgamers.com/logout");
			cargando(500); 
		espera(500);	
		System.out.println("Log out");
	}	
	
}
 
package pages;

 
import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
  
public class StressTestGrupoC extends TestBaseTG {
	
	final WebDriver driver; 
	public StressTestGrupoC(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	 
	
	  
	/*
	 ******PASAR A BASEPAGE
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	 
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "email_reg")
	private WebElement nombreUsuario;
	
	@FindBy(how = How.ID,using = "password_reg")
	private WebElement contraseñaUsuario;
	
	@FindBy(how = How.ID,using = "container-chat-open-button")
	private WebElement chatOnline;

	 
	//***************************** 

	 
	public void LogInTodosGamers41(String passAmbiente, String Usuario41) {
		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test StressTest-LogIn");
		 
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000); 
		driver.switchTo();
			nombreUsuario.sendKeys(Usuario41);
			contraseñaUsuario.sendKeys(passAmbiente);
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	   
		
		//UNIR AL TORNEO
		
	 
	}
	
public void LogInTodosGamers42(String passAmbiente, String Usuario42) {
		
		System.out.println();
		System.out.println("***************************************************************************");
		System.out.println();
		System.out.println("Inicio de Test StressTest-LogIn");
		 
		cargando(500);
		espera(1500); 
		  
		WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
		btnIngresar.click();
		
		espera(2000); 
		driver.switchTo();
			nombreUsuario.sendKeys(Usuario42);
			contraseñaUsuario.sendKeys(passAmbiente);
			WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
			btnIngresarPortal.click();		
		driver.switchTo().defaultContent();
		
		cargando(500);
		espera(500);	   
		
		//UNIR AL TORNEO
		
	 
	}

public void LogInTodosGamers43(String passAmbiente, String Usuario43) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario43);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers44(String passAmbiente, String Usuario44) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario44);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}
	

public void LogInTodosGamers45(String passAmbiente, String Usuario45) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario45);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers46(String passAmbiente, String Usuario46) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario46);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}
	

public void LogInTodosGamers47(String passAmbiente, String Usuario47) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario47);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers48(String passAmbiente, String Usuario48) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario48);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers49(String passAmbiente, String Usuario49) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario49);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers50(String passAmbiente, String Usuario50) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario50);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers51(String passAmbiente, String Usuario51) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario51);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}


public void LogInTodosGamers52(String passAmbiente, String Usuario52) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario52);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers53(String passAmbiente, String Usuario53) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario53);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers54(String passAmbiente, String Usuario54) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario54);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers55(String passAmbiente, String Usuario55) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario55);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers56(String passAmbiente, String Usuario56) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario56);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers57(String passAmbiente, String Usuario57) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario57);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers58(String passAmbiente, String Usuario58) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario58);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers59(String passAmbiente, String Usuario59) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario59);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void LogInTodosGamers60(String passAmbiente, String Usuario60) {
	
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test StressTest-LogIn");
	 
	cargando(500);
	espera(1500); 
	  
	WebElement btnIngresar = driver.findElement(By.cssSelector("a.btn.btn-primary.btn_login"));
	btnIngresar.click();
	
	espera(2000); 
	driver.switchTo();
		nombreUsuario.sendKeys(Usuario60);
		contraseñaUsuario.sendKeys(passAmbiente);
		WebElement btnIngresarPortal = driver.findElement(By.cssSelector(".btn.btn-default.btn-lg.g-recaptcha"));
		btnIngresarPortal.click();		
	driver.switchTo().defaultContent();
	
	cargando(500);
	espera(500);	   
	//UNIR AL TORNEO	
 
}

public void InscripcionTorneo() {
	espera(500);
	driver.get("https://todosgamers.com/tournaments/Lorem2445/details");
	
	WebElement btnRegistrarse = driver.findElement(By.xpath("//a[contains(text(), ' Registrarse')]"));
		btnRegistrarse.click();
		espera(900);
		
		WebElement contraseña = driver.findElement(By.id("password_btn"));
		contraseña.sendKeys("nintendo64");
		espera(900);
		
		WebElement confirmar = driver.findElement(By.id("modal-confirm-btn-confirm"));
		confirmar.click();
		espera(900);
		
}
	public void LogOutTodosGamers(String apuntaA) {	
		cargando(500); 
			espera(500);
			//WebElement btnLogOut = driver.findElement(By.cssSelector(".icon-logout"));
			//btnLogOut.click();
			driver.get(apuntaA + "todosgamers.com/logout");
			cargando(500); 
		espera(500);	
		System.out.println("Log out");
	}	
	
}
 